local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

ESX                           = nil
local GUI                     = {}
GUI.Time                      = 0
local OwnedWarehouses         = {}
local Blips                   = {}
local CurrentWarehouse         = nil
local CurrentWarehouseOwner    = nil
local LastWarehouse            = nil
local LastPart                = nil
local HasAlreadyEnteredMarker = false
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local FirstSpawn              = true
local HasChest                = false



function DrawSub(text, time)
  ClearPrints()
  SetTextEntry_2('STRING')
  AddTextComponentString(text)
  DrawSubtitleTimed(time, 1)
end



function CreateBlips()
  for i=1, #Config.Warehouses, 1 do
    local warehouse = Config.Warehouses[i]

    if warehouse.entering ~= nil then
      Blips[warehouse.name] = AddBlipForCoord(warehouse.entering.x, warehouse.entering.y, warehouse.entering.z)

      ESX.TriggerServerCallback('esx_warehouse:getWarehouseIsOwned', function(isOwned)
        local sprite = nil
        local text = nil

        if isOwned == 0 then
          sprite = 474
          text = 'free_prop'
        else
          sprite = 473
          text = 'warehouse'
        end

        SetBlipSprite(Blips[warehouse.name], sprite)
        SetBlipDisplay(Blips[warehouse.name], 4)
        SetBlipScale(Blips[warehouse.name], 1.0)
        SetBlipAsShortRange(Blips[warehouse.name], true)

        BeginTextCommandSetBlipName("STRING")
        AddTextComponentString(_U(text))
        EndTextCommandSetBlipName(Blips[warehouse.name])
      end, warehouse.name)
    end
  end
end


function GetWarehouses()
  return Config.Warehouses
end


function GetWarehouse(name)
  for i=1, #Config.Warehouses, 1 do
    if Config.Warehouses[i].name == name then
      return Config.Warehouses[i]
    end
  end
end


function EnterWarehouse(name, owner)
  local warehouse       = GetWarehouse(name)
  local playerPed      = GetPlayerPed(-1)
  CurrentWarehouse      = warehouse
  CurrentWarehouseOwner = owner

  for i=1, #Config.Warehouses, 1 do
    if Config.Warehouses[i].name ~= name then
      Config.Warehouses[i].disabled = true
    end
  end

  TriggerServerEvent('esx_warehouse:saveLastWarehouse', name)

  Citizen.CreateThread(function()
    DoScreenFadeOut(800)

    while not IsScreenFadedOut() do
      Citizen.Wait(0)
    end

    ESX.TriggerServerCallback('esx_warehouse:getWarehouseType', function(type)
      local warehouseConfig = Config.WarehouseSetup[type]
      local zones = warehouseConfig.zones

      SetEntityCoords(playerPed, zones.inside.x,  zones.inside.y,  zones.inside.z)
    end, name)

    ESX.TriggerServerCallback('esx_warehouse:getWarehouseLabel', function(label)
      DoScreenFadeIn(2000)
      DrawSub(label, 5000) --attempt to index nil value warehouse
    end, warehouse.name)
  end)
end


function ExitWarehouse(name)
  local warehouse  = GetWarehouse(name)
  local playerPed = GetPlayerPed(-1)
  local outside   = nil
  CurrentWarehouse = nil

  outside = warehouse.outside

  TriggerServerEvent('esx_warehouse:deleteLastWarehouse')

  Citizen.CreateThread(function()
    DoScreenFadeOut(800)

    while not IsScreenFadedOut() do
      Citizen.Wait(0)
    end

    SetEntityCoords(playerPed, outside.x,  outside.y,  outside.z)

    for i=1, #Config.Warehouses, 1 do
      Config.Warehouses[i].disabled = false
    end

    DoScreenFadeIn(2000)
  end)
end


function SetWarehouseOwned(name, owned)
  local warehouse     = GetWarehouse(name)
  local entering     = nil
  local enteringName = nil

  entering     = warehouse.entering --attempt to index nil value warehouse
  enteringName = warehouse.name

  if owned then
    OwnedWarehouses[name] = true

    RemoveBlip(Blips[enteringName])

    Blips[enteringName] = AddBlipForCoord(entering.x,  entering.y,  entering.z)

    SetBlipSprite(Blips[enteringName], 473)
    SetBlipAsShortRange(Blips[enteringName], true)

    BeginTextCommandSetBlipName("STRING")
    AddTextComponentString(_U('warehouse'))
    EndTextCommandSetBlipName(Blips[enteringName])
  else
    OwnedWarehouses[name] = nil

    local found = false

    for k,v in pairs(OwnedWarehouses) do
      local _warehouse = GetWarehouse(k)
    end

    if not found then
      RemoveBlip(Blips[enteringName])

      Blips[enteringName] = AddBlipForCoord(entering.x,  entering.y,  entering.z)

      SetBlipSprite(Blips[enteringName], 474)
      SetBlipAsShortRange(Blips[enteringName], true)

      BeginTextCommandSetBlipName("STRING")
      AddTextComponentString(_U('free_prop'))
      EndTextCommandSetBlipName(Blips[enteringName])
     end
  end
end


function WarehouseIsOwned(warehouse)
  return OwnedWarehouses[warehouse.name] == true
end


function OpenWarehouseMenu(warehouse)
  ESX.TriggerServerCallback('esx_warehouse:getWarehouseIsOwned', function(isOwned)
    --print(isOwned)
    if isOwned == 0 then
      print("OpenWarehouseMenuDefault")
      OpenWarehouseMenuDefault(warehouse)
    else
      print("OpenWarehouseMenuOwned")
      OpenWarehouseMenuOwned(warehouse)
    end
  end, warehouse.name)
end


function OpenWarehouseMenuDefault(warehouse)
  local elements = {}

  if not Config.EnablePlayerManagement then
    table.insert(elements, {label = _U('buy'), value = 'buy'})
  end

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'warehouse',
    {
      title = warehouse.name,
      align    = 'top-left',
      elements = elements,
    },
    function(data2, menu)
      menu.close()

      if data2.current.value == 'buy' then
        ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'enter_name', {
          title = _U('choose_name'),
        }, function(data2, menu)
          menu.close()

          local price = Config.WarehouseSetup[warehouse.type].price --how to do
          TriggerServerEvent('esx_warehouse:buyWarehouse', warehouse.name, data2.value, price)
        end, function(data2,menu)
          menu.close()
        end)
      end
    end,
    function(data, menu)
      menu.close()

      CurrentAction     = 'warehouse_menu'
      CurrentActionMsg  = _U('press_to_menu')
      CurrentActionData = {warehouse = warehouse}
    end
  )
end


function OpenWarehouseMenuOwned(warehouse)
  local elements = {}

  if WarehouseIsOwned(warehouse) then --check if client owns the warehouse
    table.insert(elements, {label = _U('enter'), value = 'enter'}) -- why is this available for warehouses we don't own?

    if not Config.EnablePlayerManagement then
      table.insert(elements, {label = _U('sell'), value = 'sell'})
    end
  else
    --[[if hasKeys then
      table.insert(elements, {label = _U('enter_as_guest'), value = 'enter_as_guest'})
    end
    if isCop_and_hasWarrant then
      table.insert(elements, {label = _U('enter_as_guest'), value = 'enter_as_guest'})
    end]]
  end

  ESX.TriggerServerCallback('esx_warehouse:getWarehouseLabel', function(label)
    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'warehouse',
      {
        title = label,
        align    = 'top-left',
        elements = elements,
      },
      function(data2, menu)
        menu.close()

        if data2.current.value == 'enter' then
          local data = {warehouse.name}
          local Instance = {
        		type = 'warehouse',
        		host = 0,
        		data = data
        	}
          TriggerEvent('instance:create', 'warehouse', {warehouse = warehouse.name, owner = ESX.GetPlayerData().identifier})
        end

        if data2.current.value == 'sell' then
          ESX.TriggerServerCallback('esx_warehouse:getWarehouseType', function(type)
            local warehouseConfig = Config.WarehouseSetup[type]

            TriggerServerEvent('esx_warehouse:removeOwnedWarehouse', warehouse.name, warehouseConfig.price) -- how does warehouse.name not return nil here?
          end, warehouse.name)
        end
      end,
      function(data, menu)
        menu.close()

        CurrentAction     = 'warehouse_menu'
        CurrentActionMsg  = _U('press_to_menu')
        CurrentActionData = {warehouse = warehouse}
      end
    )
  end, warehouse.name)
end


function OpenRoomMenu(warehouse, owner)
	local entering = warehouse.entering
	local elements = {}

	table.insert(elements, {label = _U('invite_player'),  value = 'invite_player'})

	if CurrentWarehouseOwner == owner then
		--table.insert(elements, {label = _U('player_clothes'), value = 'player_dressing'})
		--table.insert(elements, {label = _U('remove_cloth'), value = 'remove_cloth'})
    --table.insert(elements, {label = _U('install_equip'), value = 'install_equip'})
	end

	table.insert(elements, {label = _U('remove_object'),  value = 'room_inventory'})
	table.insert(elements, {label = _U('deposit_object'), value = 'player_inventory'})

	ESX.UI.Menu.CloseAll()

  ESX.TriggerServerCallback('esx_warehouse:getWarehouseLabel', function(label)
    ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'room',
  	{
  		title    = label,
  		align    = 'top-left',
  		elements = elements
  	}, function(data, menu)
  		if data.current.value == 'invite_player' then
  			local playersInArea = ESX.Game.GetPlayersInArea(entering, 10.0)
  			local elements      = {}

  			for i=1, #playersInArea, 1 do
  				if playersInArea[i] ~= PlayerId() then
  					table.insert(elements, {label = GetPlayerName(playersInArea[i]), value = playersInArea[i]})
  				end
  			end

  			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'room_invite',
  			{
  				title    = _U('invite'),
  				align    = 'top-left',
  				elements = elements,
  			}, function(data2, menu2)
  				TriggerEvent('instance:invite', 'warehouse', GetPlayerServerId(data2.current.value), {warehouse = warehouse.name, owner = owner})
  				ESX.ShowNotification(_U('you_invited', GetPlayerName(data2.current.value)))
  			end, function(data2, menu2)
  				menu2.close()
  			end)
  		elseif data.current.value == 'room_inventory' then
        --print("\nsending warehouse: " .. warehouse.name .. " to OpenRoomInventoryMenu")
  			OpenRoomInventoryMenu(warehouse, owner) --this isnt getting the right warehouse
  		elseif data.current.value == 'player_inventory' then
        --print("\nsending warehouse: " .. warehouse.name .. " to OpenPlayerInventoryMenu")
  			OpenPlayerInventoryMenu(warehouse, owner) --this isnt getting the right warehouse
      --[[elseif data.current.value == 'install_equip' then
        --print("\nsending warehouse: " .. warehouse.name .. " to InstallEquipmentMenu")
  			InstallEquipmentMenu(warehouse, owner) --this isnt getting the right warehouse]]
  		end
  	end, function(data, menu)
  		menu.close()

  		CurrentAction     = 'room_menu'
  		CurrentActionMsg  = _U('press_to_menu')
  		CurrentActionData = {warehouse = warehouse, owner = owner}
  	end)
  end, warehouse.name)
end


function OpenRoomInventoryMenu(warehouse, owner)
	ESX.TriggerServerCallback('esx_warehouse:getWarehouseInventory', function(inventory)
		local elements = {}

		if inventory.blackMoney > 0 then
			table.insert(elements, {label = _U('dirty_money', inventory.blackMoney), type = 'item_account', value = 'black_money'})
		end

		for i=1, #inventory.items, 1 do
			local item = inventory.items[i]

			if item.count > 0 then
				table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
			end
		end

		for i=1, #inventory.weapons, 1 do
			local weapon = inventory.weapons[i]
			table.insert(elements, {label = ESX.GetWeaponLabel(weapon.name) .. ' [' .. weapon.ammo .. ']', type = 'item_weapon', value = weapon.name, ammo = weapon.ammo})
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'room_inventory',
		{
			title    = _U('inventory'),
			align    = 'top-left',
			elements = elements
		}, function(data, menu)
			if data.current.type == 'item_weapon' then
				menu.close()

				TriggerServerEvent('esx_warehouse:getItem', owner, data.current.type, data.current.value, data.current.ammo)
				ESX.SetTimeout(300, function()
					OpenRoomInventoryMenu(warehouse, owner)
				end)
			else
				ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'get_item_count', {
					title = _U('amount'),
				}, function(data2, menu)

					local quantity = tonumber(data2.value)
					if quantity == nil then
						ESX.ShowNotification(_U('amount_invalid'))
					else
						menu.close()

						TriggerServerEvent('esx_warehouse:getItem', owner, data.current.type, data.current.value, quantity)
						ESX.SetTimeout(300, function()
							OpenRoomInventoryMenu(warehouse, owner)
						end)
					end
				end, function(data2,menu)
					menu.close()
				end)
			end
		end, function(data, menu)
			menu.close()
		end)
	end, owner)
end


function OpenPlayerInventoryMenu(warehouse, owner)
	ESX.TriggerServerCallback('esx_warehouse:getPlayerInventory', function(inventory)
		local elements = {}

		if inventory.blackMoney > 0 then
			table.insert(elements, {label = _U('dirty_money', inventory.blackMoney), type = 'item_account', value = 'black_money'})
		end

		for i=1, #inventory.items, 1 do
			local item = inventory.items[i]
			if item.count > 0 then
				table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
			end
		end

		local playerPed  = PlayerPedId()
		local weaponList = ESX.GetWeaponList()

		for i=1, #weaponList, 1 do
			local weaponHash = GetHashKey(weaponList[i].name)
			if HasPedGotWeapon(playerPed, weaponHash, false) and weaponList[i].name ~= 'WEAPON_UNARMED' then
				local ammo = GetAmmoInPedWeapon(playerPed, weaponHash)
				table.insert(elements, {label = weaponList[i].label .. ' [' .. ammo .. ']', type = 'item_weapon', value = weaponList[i].name, ammo = ammo})
			end
		end

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'player_inventory',
		{
			title    = _U('inventory'),
			align    = 'top-left',
			elements = elements
		}, function(data, menu)

			if data.current.type == 'item_weapon' then
				menu.close()

				TriggerServerEvent('esx_warehouse:putItem', owner, data.current.type, data.current.value, data.current.ammo)

				ESX.SetTimeout(300, function()
					OpenPlayerInventoryMenu(warehouse, owner)
				end)
			else

				ESX.UI.Menu.Open('dialog', GetCurrentResourceName(), 'put_item_count', {
					title = _U('amount'),
				}, function(data2, menu2)
					local quantity = tonumber(data2.value)

					if quantity == nil then
						ESX.ShowNotification(_U('amount_invalid'))
					else
						menu2.close()

						TriggerServerEvent('esx_warehouse:putItem', owner, data.current.type, data.current.value, tonumber(data2.value))
						ESX.SetTimeout(300, function()
							OpenPlayerInventoryMenu(warehouse, owner)
						end)
					end
				end, function(data2, menu2)
					menu2.close()
				end)
			end
		end, function(data, menu)
			menu.close()
		end)
	end)
end


--[[function InstallEquipmentMenu(warehouse, owner)
  ESX.TriggerServerCallback('esx_warehouse:getPlayerInventory', function(inventory)
    local elements = {}

    local coords = GetEntityCoords(PlayerPedId())
    local interiorID = GetInteriorAtCoords(coords)

    for i=1, #inventory.items, 1 do
      local item = inventory.items[i]
    end

    ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'install_equip',
    {
      title    = _U('equipment'),
      align    = 'top-left',
      elements = elements,
    },
    function(data, menu)
      local item = data.current.value

      menu.close()

      ESX.SetTimeout(300, function()
        InstallEquipmentMenu(warehouse, owner)
      end)
    end,
    function(data, menu)
      menu.close()
    end)
  end)
end]]

--[[function getWarehouseConfig()
  local coords = GetEntityCoords(PlayerPedId())
  local interiorID = GetInteriorAtCoords(coords)

  if Config.WarehouseSetup.Warehouse_Small.interiorID == interiorID then
    return Config.WarehouseSetup.Warehouse_Small
  elseif Config.WarehouseSetup.Warehouse_Medium.interiorID == interiorID then
    return Config.WarehouseSetup.Warehouse_Medium
  elseif Config.WarehouseSetup.Warehouse_Large.interiorID == interiorID then
    return Config.WarehouseSetup.Warehouse_Large
  end
end]]



AddEventHandler('instance:loaded', function()
  TriggerEvent('instance:registerType', 'warehouse',
    function(instance)
      EnterWarehouse(instance.data.warehouse, instance.data.owner)
    end,
    function(instance)
      ExitWarehouse(instance.data.warehouse)
    end
  )

end)


AddEventHandler('playerSpawned', function()
  if FirstSpawn then
    Citizen.CreateThread(function()
      while not ESX.IsPlayerLoaded() do
        Citizen.Wait(0)
      end

      ESX.TriggerServerCallback('esx_warehouse:getLastWarehouse', function(warehouseName)
        if warehouseName ~= nil then
          TriggerEvent('instance:create', 'warehouse', {warehouse = warehouseName, owner = ESX.GetPlayerData().identifier})
        end
      end)
    end)
    FirstSpawn = false
  end
end)


--[[AddEventHandler('esx_warehouse:resetProps', function(warehouse)
  Citizen.Trace("\nresetProps")

  while GetInteriorFromEntity(PlayerPedId()) == 0 do --if the player enters from outside, wait till shit loads
    Citizen.Wait(0)
  end

  for k,warehouseConfig in pairs(Config.WarehouseSetup) do
    local coords = GetEntityCoords(PlayerPedId())
    local interiorID = GetInteriorAtCoords(coords)
    if warehouseConfig.interiorID == interiorID then --interiorID is 0 here
      for _,prop in ipairs(warehouseConfig.props) do
        if IsInteriorPropEnabled(interiorID, prop) then
          print("DISABLE INTERIOR PROP FOR RESET: " .. prop)
          TriggerEvent('esx_warehouse:disableProp', prop, hostThisInstance)
        end
      end
    end
  end

  ESX.TriggerServerCallback('esx_warehouse:getProps', function(props, hostThisInstance)
    print("\nGot props for onEnter: " .. json.encode(props))
    for _,prop in ipairs(props) do
      TriggerEvent('esx_warehouse:enableProp', prop, hostThisInstance) --this only needs to be local
    end
  end, warehouse.name, hostThisInstance)
end)]]


AddEventHandler('esx_warehouse:getWarehouses', function(cb)
  cb(GetWarehouses())
end)


AddEventHandler('esx_warehouse:getWarehouse', function(name, cb)
  cb(GetWarehouse(name))
end)



RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
  PlayerLoaded = true
end)


RegisterNetEvent('esx_warehouse:setWarehouseOwned')
AddEventHandler('esx_warehouse:setWarehouseOwned', function(name, owned)
  SetWarehouseOwned(name, owned)
end)


RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
  ESX.TriggerServerCallback('esx_warehouse:getOwnedWarehouses', function(ownedWarehouses)
    for i=1, #ownedWarehouses, 1 do
      SetWarehouseOwned(ownedWarehouses[i], true)
    end
  end)
end)



--[[RegisterNetEvent('esx_warehouse:enableProp')
AddEventHandler('esx_warehouse:enableProp', function(prop, host)
  Citizen.Trace("\nenableProp: " .. prop)

  local coords = GetEntityCoords(PlayerPedId())
  local interiorID = GetInteriorAtCoords(coords)

  Citizen.Trace("\ninteriorID: " .. interiorID)
  if interiorID ~= 0 then
    Citizen.Trace("\nhost: " .. host) --this is nil when executed for installEquipment
    Citizen.Trace("\nhostThisInstance: " .. hostThisInstance)

    if hostThisInstance == host then  --hostThisInstance defined in instance onEnter
      if not IsInteriorPropEnabled(interiorID, prop) then
        EnableInteriorProp(interiorID, prop)
        RefreshInterior(interiorID)

        Citizen.Trace("\nEnabled Prop: " .. prop)
      else
        Citizen.Trace("\nProp already enabled: " .. prop)
      end
    end
  end
end)


RegisterNetEvent('esx_warehouse:disableProp')
AddEventHandler('esx_warehouse:disableProp', function(prop, host)
  Citizen.Trace("\ndisableProp: " .. prop)

  local coords = GetEntityCoords(PlayerPedId())
  local interiorID = GetInteriorAtCoords(coords)

  Citizen.Trace("\ninteriorID: " .. interiorID)
  if interiorID ~= 0 then
    Citizen.Trace("\nhost: " .. host)
    Citizen.Trace("\nhostThisInstance: " .. hostThisInstance)

    if hostThisInstance == host then   --hostThisInstance defined in instance onEnter
      if IsInteriorPropEnabled(interiorID, prop) then
        DisableInteriorProp(interiorID, prop)
        RefreshInterior(interiorID)

        Citizen.Trace("\nDisabled Prop: " .. prop)
      else
        Citizen.Trace("\nProp already disabled: " .. prop)
      end
    end
  end
end)]]


RegisterNetEvent('instance:onCreate')
AddEventHandler('instance:onCreate', function(instance)
  if instance.type == 'warehouse' then
    TriggerEvent('instance:enter', instance)
  end
end)


RegisterNetEvent('instance:onEnter')
AddEventHandler('instance:onEnter', function(instance)
  print("on enter")
  while GetInteriorFromEntity(PlayerPedId()) == 0 do  --if the player enters from outside, wait till shit loads
    Citizen.Wait(0)
  end

  if instance.type == 'warehouse' then
    local warehouse = GetWarehouse(instance.data.warehouse)
    local isHost   = GetPlayerFromServerId(instance.host) == PlayerId()
    Citizen.Trace("\nwarehouse: " .. tostring(warehouse))
    Citizen.Trace("\nisHost: " .. tostring(isHost))

    hostThisInstance = GetPlayerFromServerId(instance.host)
    Citizen.Trace("\nhostThisInstance: " .. hostThisInstance)

    local isOwned  = false

    if WarehouseIsOwned(warehouse) == true then
      isOwned = true
    end

    if(isOwned or not isHost) then
      HasChest = true
    else
      HasChest = false
    end

    --[[local roomMenu = getWarehouseConfig().zones.roomMenu
    roomMenuBlip = AddBlipForCoord(roomMenu.x,  roomMenu.y,  roomMenu.z)
    SetBlipSprite(roomMenuBlip, 606)
    SetBlipAsShortRange(roomMenuBlip, true)
    BeginTextCommandSetBlipName("STRING")
    AddTextComponentString(_U('room_menu'))
    EndTextCommandSetBlipName(roomMenuBlip)]]

    --TriggerEvent('esx_warehouse:resetProps', warehouse)
  end
end)


RegisterNetEvent('instance:onPlayerLeft')
AddEventHandler('instance:onPlayerLeft', function(instance, player)
  if player == instance.host then
    TriggerEvent('instance:leave')
  end
  RemoveBlip(roomMenuBlip)
end)



AddEventHandler('esx_warehouse:hasEnteredMarker', function(name, part)
  local warehouse = GetWarehouse(name)
  --print("\nwarehouse in hasEnteredMarker: " .. name)

  if part == 'entering' then
    CurrentAction     = 'warehouse_menu'
    CurrentActionMsg  = _U('press_to_menu')
    CurrentActionData = {warehouse = warehouse}
  end

  if part == 'exit' then
    CurrentAction     = 'room_exit'
    CurrentActionMsg  = _U('press_to_exit')
    CurrentActionData = {warehouseName = name}
  end

  if part == 'roomMenu' then
    CurrentAction     = 'room_menu'
    CurrentActionMsg  = _U('press_to_menu')
    CurrentActionData = {warehouse = warehouse, owner = CurrentWarehouseOwner}
  end
end)


AddEventHandler('esx_warehouse:hasExitedMarker', function(name, part)
  local warehouse = GetWarehouse(name)
  --print("\nwarehouse in hasExitedMarker: " .. name)
  ESX.UI.Menu.CloseAll()
  CurrentAction = nil
end)


-- Init
Citizen.CreateThread(function()
  while ESX == nil do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(0)
  end

  ESX.TriggerServerCallback('esx_warehouse:getWarehouses', function(warehouses)
    Config.Warehouses = warehouses
    CreateBlips()
  end)
end)


-- Display markers
Citizen.CreateThread(function()
  while true do
    Wait(0)

    local coords = GetEntityCoords(GetPlayerPed(-1))
    local interiorID = GetInteriorAtCoords(coords)


    for i=1, #Config.Warehouses, 1 do
      local warehouse = Config.Warehouses[i]

      --entering (the outside portal)
      if (not warehouse.disabled and GetDistanceBetweenCoords(coords, warehouse.entering.x, warehouse.entering.y, warehouse.entering.z, true) < Config.DrawDistance) then
        DrawMarker(Config.MarkerType, warehouse.entering.x, warehouse.entering.y, warehouse.entering.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.EnterExitMarkerColor.r, Config.EnterExitMarkerColor.g, Config.EnterExitMarkerColor.b, 100, false, true, 2, false, false, false, false)
      end

      if Config.WarehouseSetup.Warehouse_Small.interiorID == interiorID then
        local warehouseConfig = Config.WarehouseSetup.Warehouse_Small
        local zones = warehouseConfig.zones

        --exit (the inside portal)
        if (not warehouse.disabled and GetDistanceBetweenCoords(coords, zones.exit.x, zones.exit.y, zones.exit.z, true) < Config.DrawDistance) then
          DrawMarker(Config.MarkerType, zones.exit.x, zones.exit.y, zones.exit.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.EnterExitMarkerColor.r, Config.EnterExitMarkerColor.g, Config.EnterExitMarkerColor.b, 100, false, true, 2, false, false, false, false)
        end

        --room menu portal
        if (HasChest and not warehouse.disabled and GetDistanceBetweenCoords(coords, zones.roomMenu.x, zones.roomMenu.y, zones.roomMenu.z, true) < Config.DrawDistance) then
          DrawMarker(Config.MarkerType, zones.roomMenu.x, zones.roomMenu.y, zones.roomMenu.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.RoomMenuMarkerColor.r, Config.RoomMenuMarkerColor.g, Config.RoomMenuMarkerColor.b, 100, false, true, 2, false, false, false, false)
        end
      end


      if Config.WarehouseSetup.Warehouse_Medium.interiorID == interiorID then
        local warehouseConfig = Config.WarehouseSetup.Warehouse_Medium
        local zones = warehouseConfig.zones

        --exit (the inside portal)
        if (not warehouse.disabled and GetDistanceBetweenCoords(coords, zones.exit.x, zones.exit.y, zones.exit.z, true) < Config.DrawDistance) then
          DrawMarker(Config.MarkerType, zones.exit.x, zones.exit.y, zones.exit.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.EnterExitMarkerColor.r, Config.EnterExitMarkerColor.g, Config.EnterExitMarkerColor.b, 100, false, true, 2, false, false, false, false)
        end

        --room menu portal
        if (HasChest and not warehouse.disabled and GetDistanceBetweenCoords(coords, zones.roomMenu.x, zones.roomMenu.y, zones.roomMenu.z, true) < Config.DrawDistance) then
          DrawMarker(Config.MarkerType, zones.roomMenu.x, zones.roomMenu.y, zones.roomMenu.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.RoomMenuMarkerColor.r, Config.RoomMenuMarkerColor.g, Config.RoomMenuMarkerColor.b, 100, false, true, 2, false, false, false, false)
        end
      end


      if Config.WarehouseSetup.Warehouse_Large.interiorID == interiorID then
        local warehouseConfig = Config.WarehouseSetup.Warehouse_Large
        local zones = warehouseConfig.zones

        --exit (the inside portal)
        if (not warehouse.disabled and GetDistanceBetweenCoords(coords, zones.exit.x, zones.exit.y, zones.exit.z, true) < Config.DrawDistance) then
          DrawMarker(Config.MarkerType, zones.exit.x, zones.exit.y, zones.exit.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.EnterExitMarkerColor.r, Config.EnterExitMarkerColor.g, Config.EnterExitMarkerColor.b, 100, false, true, 2, false, false, false, false)
        end

        --room menu portal
        if (HasChest and not warehouse.disabled and GetDistanceBetweenCoords(coords, zones.roomMenu.x, zones.roomMenu.y, zones.roomMenu.z, true) < Config.DrawDistance) then
          DrawMarker(Config.MarkerType, zones.roomMenu.x, zones.roomMenu.y, zones.roomMenu.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.RoomMenuMarkerColor.r, Config.RoomMenuMarkerColor.g, Config.RoomMenuMarkerColor.b, 100, false, true, 2, false, false, false, false)
        end
      end
    end
  end
end)


-- Enter / Exit marker events
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(0)

    local coords          = GetEntityCoords(GetPlayerPed(-1))
    local interiorID      = GetInteriorAtCoords(coords)
    local isInMarker      = false
    local currentWarehouse = nil
    local currentPart     = nil


    for i=1, #Config.Warehouses, 1 do
      local warehouse = Config.Warehouses[i]

      --entering (the outside portal)
      if (not warehouse.disabled and GetDistanceBetweenCoords(coords, warehouse.entering.x, warehouse.entering.y, warehouse.entering.z, true) < Config.MarkerSize.x) then
        isInMarker      = true
        currentWarehouse = warehouse.name
        currentPart     = 'entering'
      end

      if (not warehouse.disabled and GetDistanceBetweenCoords(coords, warehouse.outside.x, warehouse.outside.y, warehouse.outside.z, true) < Config.MarkerSize.x) then
        isInMarker      = true
        currentWarehouse = warehouse.name
        currentPart     = 'outside'
      end

      if Config.WarehouseSetup.Warehouse_Small.interiorID == interiorID then
        local warehouseConfig = Config.WarehouseSetup.Warehouse_Small
        local zones = warehouseConfig.zones

        --room menu portal
        if (HasChest and not warehouse.disabled and GetDistanceBetweenCoords(coords, zones.roomMenu.x, zones.roomMenu.y, zones.roomMenu.z, true) < Config.MarkerSize.x) then
          isInMarker      = true
          currentWarehouse = warehouse.name
          currentPart     = 'roomMenu'
        end

        --exit (the inside portal)
        if (not warehouse.disabled and GetDistanceBetweenCoords(coords, zones.exit.x, zones.exit.y, zones.exit.z, true) < Config.MarkerSize.x) then
          isInMarker      = true
          currentWarehouse = warehouse.name
          currentPart     = 'exit'
        end
      end


      if Config.WarehouseSetup.Warehouse_Medium.interiorID == interiorID then
        local warehouseConfig = Config.WarehouseSetup.Warehouse_Medium
        local zones = warehouseConfig.zones

        --room menu portal
        if (HasChest and not warehouse.disabled and GetDistanceBetweenCoords(coords, zones.roomMenu.x, zones.roomMenu.y, zones.roomMenu.z, true) < Config.MarkerSize.x) then
          isInMarker      = true
          currentWarehouse = warehouse.name
          currentPart     = 'roomMenu'
        end

        --exit (the inside portal)
        if (not warehouse.disabled and GetDistanceBetweenCoords(coords, zones.exit.x, zones.exit.y, zones.exit.z, true) < Config.MarkerSize.x) then
          isInMarker      = true
          currentWarehouse = warehouse.name
          currentPart     = 'exit'
        end
      end


      if Config.WarehouseSetup.Warehouse_Large.interiorID == interiorID then
        local warehouseConfig = Config.WarehouseSetup.Warehouse_Large
        local zones = warehouseConfig.zones

        --room menu portal
        if (HasChest and not warehouse.disabled and GetDistanceBetweenCoords(coords, zones.roomMenu.x, zones.roomMenu.y, zones.roomMenu.z, true) < Config.MarkerSize.x) then
          isInMarker      = true
          currentWarehouse = warehouse.name
          currentPart     = 'roomMenu'
        end

        --exit (the inside portal)
        if (not warehouse.disabled and GetDistanceBetweenCoords(coords, zones.exit.x, zones.exit.y, zones.exit.z, true) < Config.MarkerSize.x) then
          isInMarker      = true
          currentWarehouse = warehouse.name
          currentPart     = 'exit'
        end
      end
    end

    if isInMarker and not HasAlreadyEnteredMarker or (isInMarker and (LastWarehouse ~= currentWarehouse or LastPart ~= currentPart)) then
      HasAlreadyEnteredMarker = true
      LastWarehouse            = currentWarehouse
      LastPart                = currentPart

      print("\nEnter/Exit marker events currentWarehouse: " .. currentWarehouse)
      print("\nEnter/Exit marker events currentPart: " .. currentPart)
      TriggerEvent('esx_warehouse:hasEnteredMarker', currentWarehouse, currentPart)
    end

    if not isInMarker and HasAlreadyEnteredMarker then
      HasAlreadyEnteredMarker = false
      TriggerEvent('esx_warehouse:hasExitedMarker', LastWarehouse, LastPart)
    end
  end
end)


-- Key controls
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(0)

    local coords = GetEntityCoords(PlayerPedId())
    local interiorID = GetInteriorAtCoords(coords)

    if CurrentAction ~= nil then
      ESX.ShowHelpNotification(CurrentActionMsg)

      if IsControlJustReleased(0, Keys['E']) then
        --Warehouses
        if CurrentAction == 'warehouse_menu' then
          --print("\nsending warehouse: " .. CurrentActionData.warehouse.name .. " to OpenWarehouseMenu")
          OpenWarehouseMenu(CurrentActionData.warehouse)
        elseif CurrentAction == 'room_menu' then -- this is triggering 3 times?
          --print("\nsending warehouse: " .. CurrentActionData.warehouse.name .. " to OpenRoomMenu")
          OpenRoomMenu(CurrentActionData.warehouse, CurrentActionData.owner)
        elseif CurrentAction == 'room_exit' then
          TriggerEvent('instance:leave')
        end

        CurrentAction = nil
      end
    else -- no current action, sleep mode
      Citizen.Wait(500)
    end
  end
end)
