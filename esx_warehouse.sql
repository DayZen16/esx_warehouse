USE `essentialmode`;

ALTER TABLE `users`
  ADD COLUMN `last_warehouse` VARCHAR(255) NULL
;

INSERT INTO `addon_account` (name, label, shared) VALUES
  ('warehouse_black_money','Dirty Money',0)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
  ('warehouse','Warehouse',0)
;

INSERT INTO `datastore` (name, label, shared) VALUES
  ('warehouse','Warehouse',0)
;

CREATE TABLE `owned_warehouses` (

  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `label` varchar(255) NOT NULL,
  `type` varchar(45) DEFAULT NULL,
  `owner` varchar(60) NOT NULL,
  `props` varchar(255) DEFAULT '[]',

  PRIMARY KEY (`id`)
);

CREATE TABLE `warehouses` (

  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `entering` varchar(255) DEFAULT NULL,
  `outside` varchar(255) DEFAULT NULL,
  `is_owned` int(11) DEFAULT '[]',

  PRIMARY KEY (`id`)
);